from nose.tools import assert_equal
import math
import aims

def test_ints():
    numbers = [1, 2, 3, 4, 5]
    obs = aims.std(numbers)
    exp = math.sqrt(2)
    assert_equal(obs, exp)

def test_double():
    # This one will fail in Python 2
    num_list=[1,2,3,4]
    obs = aims.std(num_list)
    exp = math.sqrt(1.25)
    assert_equal(obs, exp)

def test_neg():
    numbers = [-1, -2, -3, -4, -5]
    obs = aims.std(numbers)
    exp = math.sqrt(2)
    assert_equal(obs, exp)

def test_double_neg():
    # This one will fail in Python 2
    num_list=[-1, -2, -3, -4]
    obs = aims.std(num_list)
    exp = math.sqrt(1.25)
    assert_equal(obs, exp)

def test_one():
    filenames = ['data/bert/audioresult-00215', 'data/bert/audioresult-00222']
    obs = aims.avg_range(filenames)
    exp = 5
    assert_equal(obs, exp)

def test_two():
    filenames = ['data/alexander/data_216.DATA', 'data/alexander/data_278.DATA', 'data/alexander/data_309.DATA']
    obs = aims.avg_range(filenames)
    exp = 5
    assert_equal(obs, exp)

def test_three():
    filenames = ['data/gerdal/Data0211', 'data/gerdal/Data0232', 'data/gerdal/Data0257']
    obs = aims.avg_range(filenames)
    exp = 23/3.0
    assert_equal(obs, exp)

